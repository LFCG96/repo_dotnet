﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_CRUD_MVC.Models
{
    [MetadataType(typeof(Employee.MetaData))] //Para hacer match con la otra clase :D
    public partial class Employee
    {
        sealed class MetaData //Sealed para que no se pueda instanciar 
        {
            [Required(ErrorMessage = "Ingresa Nombre")]
            public string Empname;

            [Required]
            [EmailAddress(ErrorMessage ="Ingresa email válido")]
            public string Email;

            [Required]
            [Range(20,50,ErrorMessage ="Ingresa edad entre 20 y 50")]
            public Nullable<int> Age;

            [Required(ErrorMessage ="Ingresa salario")]
            public Nullable<int> Salary;

        }
    }
}