﻿using ProyectoMVCFramework1.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;

namespace ProyectoMVCFramework1.Models
{
    public class Login
    {
        [Required]
        [EmailAddress(ErrorMessage = "Usernames must be a valid email address.")]
        public string Username { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "This password is too short.")]
        [CommonPasswords(ErrorMessage = "This Password is too common.")]
        public string Password { get; set; }
    }
}