﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProyectoMVCFramework1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes(); //Para usar enrutamiento por atributos

            routes.MapRoute( //tabla de enrutamiento
                name: "Default",  //Tabla default
                url: "{controller}/{action}/{id}", //La ruta será controlador/acción/id
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional } 
                //Por default es Home/Index con id opcional
            );


        }
    }
}
