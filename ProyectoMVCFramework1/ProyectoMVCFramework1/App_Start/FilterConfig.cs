﻿using ProyectoMVCFramework1.Filters;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());//Maneja cualquier exception no manejada (Se comenta porque ya hicimos nuestro propio manejador de errores CustomExceptionHandler)
            filters.Add(new LogRequestFilter()); //Agrega nuestro filtro de log de manera global 
            filters.Add(new CustomExceptionHandler());
        }
    }
}
