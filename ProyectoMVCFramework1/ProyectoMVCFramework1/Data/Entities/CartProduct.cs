namespace ProyectoMVCFramework1.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CartProduct
    {
        public int CartProductId { get; set; }

        public int CartId { get; set; }

        public int ProductId { get; set; }

        public virtual Cart Cart { get; set; } // Un cartProduct solo se le asocia un Cart

                                                    //Recordando que es una tabla "intermedia" para lograr el many to many entre Cart y Product
        public virtual Product Product { get; set; } //Un cartProduct solo se le asocia un Product
    }
}
