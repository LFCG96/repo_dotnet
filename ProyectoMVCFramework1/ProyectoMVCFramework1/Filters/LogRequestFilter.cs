﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Filters
{
    public class LogRequestFilter : ActionFilterAttribute //Implementa para ser un ActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext) //Override porque quiero que se ejecute antes OnActionExecuting
        {
            //Creamos un objeto para llevar el log
            var log = new
            {
                Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                Action = filterContext.ActionDescriptor.ActionName,
                IP = filterContext.HttpContext.Request.UserHostAddress,
                DateTime = filterContext.HttpContext.Timestamp
            };

            Debug.WriteLine(JsonConvert.SerializeObject(log));
            //Para que este filtro sea aplicado tiene que registrarse como attrib en algun metodo o controlador
            //O bien de manera general en App_Start/FilterConfig
        }
    }
}