﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Filters
{
    public class CrawlerFilter :ActionFilterAttribute
    {
        /*public override void OnActionExecuting(ActionExecutingContext filterContext) //Se ejecuta antes para que si es un crawler se pueda detener el request a tiempo
        {
            if (filterContext.HttpContext.Request.Browser.Crawler) //True si el cliente es un rastreador conocido
            {
               filterContext.Result = new HttpNotFoundResult(); //Para no dar acceso al sitio
                //Se puede agregar para todo el sitio en App_Start/FilterContext o para determinado componente 
                //Por ejemplo, lo agregaremos solo al HomeController
            }
        }*/

        public override void OnActionExecuting(ActionExecutingContext filterContext) 
        {
            if (filterContext.HttpContext.Request.Browser.Crawler) 
            {
                var path = filterContext.HttpContext.Server.MapPath("/error/404.html"); 
                var bytes = System.IO.File.ReadAllBytes(path);
                filterContext.Result = new FileContentResult(bytes, "text/html");
            }
        }
    }
}