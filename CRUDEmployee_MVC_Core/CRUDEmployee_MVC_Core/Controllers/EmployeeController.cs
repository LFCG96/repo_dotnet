﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRUDEmployee_MVC_Core.Models;
using Microsoft.EntityFrameworkCore;

namespace CRUDEmployee_MVC_Core.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApplicationDbContext _db; //Para acceder a la base de datos por medio de EF

        public EmployeeController(ApplicationDbContext db) //Constructor
        {
            _db = db;
        }

        public IActionResult Index() //En INDEX haremos que liste a todos los empleados en la BD (READ)
        {
            var displaydata = _db.Employee.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string empSearch) 
        {
            ViewData["GetEmployeesDetails"] = empSearch;//Dato compartido con la vista
            var empquery = from x in _db.Employee select x; //Query de EF, de la tabla Employee almacena en x
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x=>x.Empname.Contains(empSearch)|| x.Email.Contains(empSearch)); //Agrega claususla Where
            }
            return View(await empquery.AsNoTracking().ToListAsync()); //Retorna el resultado del query como una lista asincrona que no pasa por _db
        }

        public IActionResult Create() // Http get, para mostrar la vista de crear
        {
            return View();
        }

        [HttpPost] //Http post, para guardar los datos en la BD
        public async Task<IActionResult> Create(Employee nEmp)//Es async para no recargar la página inmediatamente 
        {
            if (ModelState.IsValid) //Si no hay errores con los DataAnnotations...
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }

        public async Task<IActionResult> Edit(int? id) //Async porque debe buscar en la BD
        {
            if (id==null) 
            {
                return RedirectToAction("Index");
            }
            var getEmpDetails = await _db.Employee.FindAsync(id);
            return View(getEmpDetails);
        }

        [HttpPost] 
        public async Task<IActionResult> Edit(Employee oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Detail(int? id) 
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetails = await _db.Employee.FindAsync(id);
            return View(getEmpDetails);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetails = await _db.Employee.FindAsync(id);
            return View(getEmpDetails);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpDetails = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpDetails);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
