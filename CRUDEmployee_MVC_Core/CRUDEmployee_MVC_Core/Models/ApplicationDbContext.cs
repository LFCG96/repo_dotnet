﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CRUDEmployee_MVC_Core.Models
{
    public class ApplicationDbContext : DbContext 
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)  
        { 
        }

        public DbSet<Employee> Employee { get; set; }
        //<Entity> y el nombre debe ser el nombre de la tabla en la BD

    }
}

